module gitlab.com/will-p/grafynamo

go 1.14

require (
	github.com/aws/aws-sdk-go v1.34.18
	github.com/grafana/grafana-plugin-sdk-go v0.77.0
	github.com/guregu/dynamo v1.9.1
	github.com/magefile/mage v1.10.0 // indirect
)
