package handlers

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"github.com/grafana/grafana-plugin-sdk-go/backend"
	"github.com/grafana/grafana-plugin-sdk-go/backend/log"
	"github.com/grafana/grafana-plugin-sdk-go/data"
	"gitlab.com/will-p/grafynamo/pkg/db"
	"gitlab.com/will-p/grafynamo/pkg/models"
)

var logger = log.DefaultLogger

// QueryHandler : a datasource query handler
type QueryHandler struct {
	Datasource *db.Dynamo
}

// QueryData : the query implementation
func (h *QueryHandler) QueryData(
	ctx context.Context,
	req *backend.QueryDataRequest,
) (*backend.QueryDataResponse, error) {
	res := backend.NewQueryDataResponse()

	for _, dq := range req.Queries {
		res.Responses[dq.RefID] = h.query(ctx, dq)
	}

	return res, nil
}

func (h *QueryHandler) query(
	ctx context.Context,
	query backend.DataQuery,
) backend.DataResponse {
	var q models.Query
	response := backend.DataResponse{}

	// Unmarshal the request body into a `models.Query`
	err := json.Unmarshal(query.JSON, &q)
	if err != nil {
		logger.Error(err.Error())
		response.Error = err
		return response
	}

	from := query.TimeRange.From
	to := query.TimeRange.To

	// Query DynamoDB. Rows will be []map[string]interface{}
	rows, err := h.Datasource.Query(q, from, to)
	if err != nil {
		logger.Error(err.Error())
		response.Error = err
		return response
	}

	// Establish target value collections
	hashes := make([]string, len(rows))
	timestamps := make([]time.Time, len(rows))
	values := make([]float64, len(rows))

	for i, row := range rows {
		// Assume `hash` is a string
		hash, ok := row[q.HashKey].(string)
		if !ok {
			msg := fmt.Sprintf(
				"The hash column type (%T) is incorrect.", row[q.HashKey],
			)
			err = errors.New(msg)
			response.Error = err
			return response
		}

		// Assume `range` is a unix timestamp
		timestamp, ok := row[q.RangeKey].(float64)
		if !ok {
			msg := fmt.Sprintf(
				"The range column type (%T) is incorrect.", row[q.RangeKey],
			)
			err = errors.New(msg)
			response.Error = err
			return response
		}

		// Assume `value` is a float
		value, ok := row[q.ValueKey].(float64)
		if !ok {
			msg := fmt.Sprintf(
				"The value column type (%T) is incorrect.", row[q.ValueKey],
			)
			err = errors.New(msg)
			response.Error = err
			return response
		}

		hashes[i] = hash
		timestamps[i] = time.Unix(int64(timestamp), 0)
		values[i] = value
	}

	// NOTE: if the data type is unsupported, an error occurs within
	// `NewField()` but is not returned or logged unless debug logging is on.
	// ¯\_(ツ)_/¯
	hashField := data.NewField(q.HashKey, nil, hashes)
	timestampField := data.NewField(q.RangeKey, nil, timestamps)
	valueField := data.NewField(q.ValueKey, nil, values)

	frame := data.NewFrame("response")
	frame.Fields = append(frame.Fields, hashField, timestampField, valueField)
	response.Frames = append(response.Frames, frame)

	return response
}
