package handlers

import (
	"context"
	"fmt"

	"github.com/grafana/grafana-plugin-sdk-go/backend"
	"gitlab.com/will-p/grafynamo/pkg/db"
)

// HealthHandler : a datasource health check handler
type HealthHandler struct {
	Datasource *db.Dynamo
}

// CheckHealth : implements a health check by attempting to list tables
func (h *HealthHandler) CheckHealth(
	ctx context.Context,
	req *backend.CheckHealthRequest,
) (*backend.CheckHealthResult, error) {
	status := backend.HealthStatusOk
	message := "Datasource access is OK"

	// like SELECT 1
	tables, err := h.Datasource.ListTables()
	if err != nil {
		status = backend.HealthStatusError
		message = fmt.Sprintf("Datasource access failed: %v", err)
	}

	res := &backend.CheckHealthResult{
		Status:  status,
		Message: message + fmt.Sprintf(" - found %d tables", len(tables)),
	}
	return res, nil
}
