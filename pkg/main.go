package main

import (
	"os"

	"github.com/grafana/grafana-plugin-sdk-go/backend/datasource"
	"github.com/grafana/grafana-plugin-sdk-go/backend/log"
	"gitlab.com/will-p/grafynamo/pkg/plugin"
)

var logger = log.DefaultLogger

func main() {
	// Create the plugin
	options, err := plugin.New()
	if err != nil {
		logger.Error(err.Error())
		os.Exit(1)
	}

	// Serve the plugin
	err = datasource.Serve(*options)
	if err != nil {
		logger.Error(err.Error())
		os.Exit(1)
	}
}
