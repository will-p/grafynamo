package models

// Query : a model of a dynamodb query
type Query struct {
	Table        string `json:"table"`
	HashKey      string `json:"hash_key"`
	HashValue    string `json:"hash_value"`
	HashKeyType  string `json:"hash_key_type"`
	RangeKey     string `json:"range_key"`
	RangeKeyType string `json:"range_key_type"`
	ValueKey     string `json:"value_key"`
}
