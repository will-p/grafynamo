package plugin

import (
	"github.com/grafana/grafana-plugin-sdk-go/backend/datasource"
	"gitlab.com/will-p/grafynamo/pkg/db"
	"gitlab.com/will-p/grafynamo/pkg/handlers"
)

// New : a new datasource plugin
func New() (*datasource.ServeOpts, error) {
	// Create an instance of the DynamoDB client
	source, err := db.New()
	if err != nil {
		return nil, err
	}

	// Create an instance of `ServeOpts` from query and health handlers
	query := handlers.QueryHandler{Datasource: source}
	health := handlers.HealthHandler{Datasource: source}
	opts := datasource.ServeOpts{
		QueryDataHandler:   &query,
		CheckHealthHandler: &health,
	}

	return &opts, nil
}
