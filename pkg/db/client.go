package db

import (
	"fmt"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/grafana/grafana-plugin-sdk-go/backend/log"
	"gitlab.com/will-p/grafynamo/pkg/models"
)

var logger = log.DefaultLogger

// New : returns a new DynamoDB client
func New() (*Dynamo, error) {
	// TODO: conditionally load credentials
	// - via plugin options
	// - via shared credentials / iam role

	// This only points to DynamoDB local. Note that if the shared DB flag is
	// not passed to the container, the key ID and secret key must match those
	// used elsewhere.
	creds := credentials.NewStaticCredentials(
		"notarealkeyid",
		"notarealaccesskey",
		"",
	)
	config := aws.Config{
		Credentials: creds,
		Region:      aws.String("us-east-1"),
		Endpoint:    aws.String("http://0.0.0.0:8000"),
	}
	sess, err := session.NewSession(&config)
	if err != nil {
		logger.Error(err.Error())
		return nil, err
	}
	return &Dynamo{client: dynamodb.New(sess)}, nil
}

// Dynamo : a DynamoDB client wrapper
type Dynamo struct {
	client *dynamodb.DynamoDB
}

// ListTables : returns a list of all tables in Dynamo DB
func (d *Dynamo) ListTables() ([]string, error) {
	tables := []string{}
	input := &dynamodb.ListTablesInput{}

	for {
		res, err := d.client.ListTables(input)
		if err != nil {
			logger.Error(err.Error())
			return tables, err
		}

		for _, n := range res.TableNames {
			tables = append(tables, *n)
		}

		if res.LastEvaluatedTableName == nil {
			break
		}
		input.ExclusiveStartTableName = res.LastEvaluatedTableName
	}

	logger.Info(fmt.Sprintf("ListTables(): %v", tables))

	return tables, nil
}

// Query : query a table
func (d *Dynamo) Query(
	q models.Query,
	from time.Time,
	to time.Time,
) ([]map[string]interface{}, error) {
	// Query DynamoDB by:
	// 1. constructing a basic key condition query expression
	// 2. assigning expression names and values using `models.Query`, `from`,
	// and `to`
	logger.Debug(fmt.Sprintf("API Query: %+v, From: %v, To: %v", q, from, to))

	var result []map[string]interface{}

	exp := "#hk = :hk and #rk between :upper and :lower"
	var hk dynamodb.AttributeValue
	var upper dynamodb.AttributeValue
	var lower dynamodb.AttributeValue
	names := map[string]*string{
		"#hk": aws.String(q.HashKey),
		"#rk": aws.String(q.RangeKey),
	}

	// TODO: it would be nice to interrogate the table for it's column types
	if strings.ToLower(q.HashKeyType) == "n" {
		hk = dynamodb.AttributeValue{N: aws.String(q.HashValue)}
	} else {
		hk = dynamodb.AttributeValue{S: aws.String(q.HashValue)}
	}

	if strings.ToLower(q.RangeKeyType) == "n" {
		// must be unix timestamp
		start := fmt.Sprintf("%d", from.Unix())
		end := fmt.Sprintf("%d", to.Unix())
		upper = dynamodb.AttributeValue{N: aws.String(start)}
		lower = dynamodb.AttributeValue{N: aws.String(end)}
	} else {
		// must be ISO 8601
		start := from.UTC().Format(time.RFC3339)
		end := to.UTC().Format(time.RFC3339)
		upper = dynamodb.AttributeValue{S: aws.String(start)}
		lower = dynamodb.AttributeValue{S: aws.String(end)}
	}

	values := map[string]*dynamodb.AttributeValue{
		":hk":    &hk,
		":upper": &upper,
		":lower": &lower,
	}
	query := &dynamodb.QueryInput{
		TableName:                 aws.String(q.Table),
		KeyConditionExpression:    aws.String(exp),
		ExpressionAttributeNames:  names,
		ExpressionAttributeValues: values,
	}

	logger.Debug(fmt.Sprintf("Dynamo Query: %+v", query))

	res, err := d.client.Query(query)
	if err != nil {
		logger.Error(err.Error())
		return nil, err
	}

	logger.Debug(fmt.Sprintf("got %d results", len(res.Items)))

	err = dynamodbattribute.UnmarshalListOfMaps(res.Items, &result)
	if err != nil {
		logger.Error(err.Error())
		return nil, err
	}

	return result, nil
}
