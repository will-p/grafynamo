#! /usr/bin/env bash

set -e
clear

export AWS_REGION="us-east-1"
export AWS_ACCESS_KEY_ID="notarealkeyid"
export AWS_SECRET_ACCESS_KEY="notarealaccesskey"

export NOW=$(date +%s) # unix now
export START=$(($NOW-86400)) # 1 day ago
export INTERVAL=60 # seconds between sample intervals
export CHUNK=25 # dynamodb batch batch write size
export ENDPOINT="http://localhost:8000"
export TABLE="grafynamo-metrics"

export HASH_KEY="name"
export RANGE_KEY="timestamp"
export VALUE_KEY="value"

# start dynamodb local with:
#  - the shared db flag
#  - host network attachment
docker run -d \
    --net=host \
    --name=dynamodb \
    amazon/dynamodb-local -jar DynamoDBLocal.jar -sharedDb &> /dev/null
printf "waiting for dynamodb"
while ! nc -z 0.0.0.0 8000; do echo -n '.'; sleep 3; done
printf " done\n"

# start grafana with:
# - this repo mounted to the plugins directory
# - the test datasource file mounted as the provisioned datasources
# - host network attachment
docker run -d \
    --net=host \
    --name=grafana \
    -e "GF_PLUGINS_ALLOW_LOADING_UNSIGNED_PLUGINS=grafynamo" \
    -v $(pwd):/var/lib/grafana/plugins \
    -v $(pwd)/test-datasource.yaml:/etc/grafana/provisioning/datasources/datasources.yaml \
    grafana/grafana:7.0.0 &> /dev/null
printf "waiting for grafana"
while ! nc -z 0.0.0.0 3000; do echo -n '.'; sleep 3; done
printf " done\n"


# create the metrics table
printf "creating $TABLE..."
aws dynamodb --endpoint-url $ENDPOINT \
    create-table --table-name $TABLE \
    --key-schema \
        AttributeName=$HASH_KEY,KeyType=HASH \
        AttributeName=$RANGE_KEY,KeyType=RANGE \
    --attribute-definitions \
        AttributeName=$HASH_KEY,AttributeType=S \
        AttributeName=$RANGE_KEY,AttributeType=N \
    --provisioned-throughput \
        ReadCapacityUnits=5,WriteCapacityUnits=5 &> /dev/null
printf " done\n"


# generate cpu and mem metric rows for every interval.
printf "generating metrics"
items=()
pi=$(echo "scale=10; 4*a(1)" | bc -l)
for (( i = $START; i < $NOW; i += $INTERVAL )); do
    printf "."
    # generate a random offset number in a range, used as jitter
    cpu_offset=$(echo "scale=2;$RANDOM/32768*(0.9-0.4) + 0.4" | bc -l)
    mem_offset=$(echo "scale=2;$RANDOM/32768*(0.5-0.3) + 0.3" | bc -l)

    # generate a value on a sine wave
    cpu=$(echo "scale=2;50*s($cpu_offset * $i / 1000)+50" | bc -l)
    mem=$(echo "scale=2;50*s($mem_offset * $i / 1500)+50" | bc -l)

    # create put requests
    items+=('{"PutRequest": {"Item": {"'$HASH_KEY'": {"S": "cpu.usage"}, "'$RANGE_KEY'": {"N": "'$i'"}, "'$VALUE_KEY'": {"N": "'$cpu'"}}}}')
    items+=('{"PutRequest": {"Item": {"'$HASH_KEY'": {"S": "mem.usage"}, "'$RANGE_KEY'": {"N": "'$i'"}, "'$VALUE_KEY'": {"N": "'$mem'"}}}}')
done
printf " done\n"


printf "writing $(( ( ($NOW-$START) / $INTERVAL ) * 2 )) rows in $(( ${#items[@]} / $CHUNK )) batches"

# chunk rows for batch writing. otherwise, it would take forever to seed the 
# database. note the `&` at the end of the last command and the `wait` that
# follows. this is like the bash equivalent of `Promise.all()`, sort of.
for (( i = 0; i < ${#items[@]}; i += $CHUNK )); do
    printf "."
    chunk=("${items[@]:i:CHUNK}")

    # join the array with ','
    joined=$(
        printf '%s\n' "$(IFS=,; printf '%s' "${chunk[*]}")"
    )
    
    # create a uuid for file identification and write the file
    uuid=$(cat /proc/sys/kernel/random/uuid)
    echo '{ "'$TABLE'": [ '$joined' ] }' > "./seed-$uuid.json"

    # perform the batch write, in the background
    batch_write_result=$(
        aws dynamodb batch-write-item --endpoint-url $ENDPOINT \
            --request-items "file://seed-$uuid.json"
    ) &

    # if your cpu gets crushed in this loop, uncomment the next 3 lines
    # if [[ $(($i % 500)) == 0 ]]; then
    #     wait
    # fi
done

# wait for all the batch writes to finish
wait
printf " done\n"

# clean up the mess
rm ./seed-*.json

printf "seeding complete\n"

