.PHONY: build run
.ONESHELL: build run

shell := /bin/bash

setup:
	yarn install

build:
	yarn build
	mage -v

clean: stop
	-mage clean
	-rm ./seed-*.json

run:
	./scripts/run-local.sh

stop:
	-docker rm -f grafana
	-docker rm -f dynamodb