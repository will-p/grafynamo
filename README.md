# Grafynamo - DynamoDB Data Source for Grafana

This is a quick and dirty POC for an AWS DynamoDB data source and is in no way ready for production use. There are **no tests** and minimal commenting.

## Prerequisites

Ensure the following are installed and usable:

- `golang`
- `node` and `yarn`
- `docker`
- `make`
- `bash`

## For Impatient Humans

1. `make setup && make build && make run`
2. `http://0.0.0.0:3000`
3. login as `admin:admin`
4. navigate to `Dashboards` > `Manage` > `New Dashboard`
5. click `Add Panel`

You should see a single line graph with the `Hash Value` field showing `cpu.usage`. If desired, add another query and change `Hash Value` to `mem.usage`.

## Build

```BASH
make setup
make build
```

## Run

```BASH
make run
```

## Stop / Clean-Up

```BASH
make stop
make clean
```

## Details

### Current State

- The backend points to DynamoDB local only
- The frontend has no configuration options
- The plugin expects the following of a DynamoDB table:
  - The table has both a `hash` and a `range` key
  - The table has a numeric column to be used as a metric value
  - The `hash` key is a `string`
  - The `range` key is a `number`
  - The value column is a `number`

### What's Going On

To see this plugin in action you need:

- a running and seeded instance of **DynamoDB**
- a running and configured instance of **Grafana**

The aforementioned `make run` target executes `scripts/run-local.sh` which does the following:

- starts a `grafana` container with:
  - this repo mounted to the plugins directory
  - a datasource config file mounted so that `Grafynamo` is ready to go as soon as you log into the frontend
- starts a vanilla `dynamodb-local` container
- creates a metrics table called `grafynamo-metrics`
- generates 24 hour's worth of two metrics (cpu and memory usage) sampled every 60 seconds
- writes the metrics, in batches, to Dynamo DB

## License

Apache 2.0, see `LICENSE`
