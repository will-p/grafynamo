import { Query } from './types';
import { DataSource } from './DataSource';
import { DataSourcePlugin } from '@grafana/data';
import { QueryEditor, ConfigEditor } from './components';

export const plugin = new DataSourcePlugin<DataSource, Query>(DataSource)
  .setConfigEditor(ConfigEditor)
  .setQueryEditor(QueryEditor);
