import { Query } from './types';
import { DataSourceWithBackend as Backend } from '@grafana/runtime';
import { DataSourceInstanceSettings as Settings } from '@grafana/data';

export class DataSource extends Backend<Query> {
  constructor(settings: Settings) {
    super(settings);
  }
}
