import defaults from 'lodash/defaults';
import { LegacyForms } from '@grafana/ui';
import { DataSource } from '../DataSource';
import React, { ChangeEvent, PureComponent } from 'react';
import { QueryEditorProps as QueryProps } from '@grafana/data';
import { defaultQuery, Query, QueryFields } from '../types';

const { FormField } = LegacyForms;

type Props = QueryProps<DataSource, Query>;

export class QueryEditor extends PureComponent<Props> {
  onFieldChange = (field: QueryFields, event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    const { query, onChange, onRunQuery } = this.props;
    query[field] = value;
    onChange(query);
    onRunQuery();
  };

  render() {
    const query = defaults(this.props.query, defaultQuery);
    const {
      table,
      hash_key,
      hash_value,
      hash_key_type,
      range_key,
      range_key_type,
      value_key,
    } = query;

    return (
      <div className="gf-form gf-form--alt">
        <div className="gf-form-inline gf-form-inline--no-wrap">
          <FormField
            label="Table"
            width={10}
            value={table}
            type="string"
            onChange={e => this.onFieldChange('table', e)}
          />
        </div>

        <div className="gf-form-inline gf-form-inline--no-wrap">
          <FormField
            label="Hash Key"
            width={10}
            value={hash_key}
            type="string"
            onChange={e => this.onFieldChange('hash_key', e)}
          />
          <FormField
            label="Hash Value"
            width={10}
            value={hash_value}
            type="string"
            onChange={e => this.onFieldChange('hash_value', e)}
          />
          <FormField
            label="Hash Key Type"
            width={10}
            value={hash_key_type}
            type="string"
            onChange={e => this.onFieldChange('hash_key_type', e)}
          />
        </div>

        <div className="gf-form-inline gf-form-inline--no-wrap">
          <FormField
            label="Range Key"
            width={10}
            value={range_key}
            type="string"
            onChange={e => this.onFieldChange('range_key', e)}
          />
          <FormField
            label="Range Key Type"
            width={10}
            value={range_key_type}
            type="string"
            onChange={e => this.onFieldChange('range_key_type', e)}
          />
        </div>

        <div className="gf-form-inline gf-form-inline--no-wrap">
          <FormField
            label="Value Key"
            width={10}
            value={value_key}
            type="string"
            onChange={e => this.onFieldChange('value_key', e)}
          />
        </div>
      </div>
    );
  }
}
