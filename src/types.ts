import { DataQuery } from '@grafana/data';

export type QueryFields =
  | 'table'
  | 'hash_key'
  | 'hash_value'
  | 'hash_key_type'
  | 'range_key'
  | 'range_key_type'
  | 'value_key';

export interface Query extends DataQuery {
  table?: string;
  hash_key?: string;
  hash_value?: string;
  hash_key_type?: string;
  range_key?: string;
  range_key_type?: string;
  value_key?: string;
}

export const defaultQuery: Partial<Query> = {
  table: 'grafynamo-metrics',
  hash_key: 'name',
  hash_value: 'cpu.usage',
  hash_key_type: 'S',
  range_key: 'timestamp',
  range_key_type: 'N',
  value_key: 'value',
};
